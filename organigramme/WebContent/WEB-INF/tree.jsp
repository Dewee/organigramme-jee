<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<li>
	<a href="personne?id=${chef.id}"> ${chef.prenom} ${chef.nom}</a>
	<c:if test="${not empty chef.descendants }">
	    <ul>
	        <c:forEach var="descendant" items="${chef.descendants}">
	        	<c:set var="chef" value="${descendant}" scope="request" />
	        	<jsp:include page="tree.jsp"/>
	        </c:forEach>
	    </ul>
    </c:if>
</li>
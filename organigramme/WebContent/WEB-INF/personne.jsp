<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title><p> ${personne.prenom } ${personne.nom} </p> </title>
        <link rel=stylesheet type="text/css" href="style.css" />
</head>
<body>
	<c:if test="${ !empty sessionScope.identifiant }">
			<div class="identification">
			
			<p>Connect� en tant que ${ sessionScope.prenom  } ${ sessionScope.nom  }
			<br/> Derni�re connexion : ${ sessionScope.lastConnexion }
			<br/> Nombre de connexion : ${ sessionScope.nbConnexion }  !
			</p>
			<form method="post" action="Accueil">
          	<input type="submit" id="se d�connecter" value="Se d�connecter"/>
       		</form>
       		</div>
       		
		</c:if>
		<div class="corps">
	<h1 class="titre">${personne.nom} ${personne.prenom}</h1>
	<c:choose>
		<c:when test="${sessionScope.identifiant == personne.username}">
			<form method="post" action="personne?id=${personne.id}">
			<ul class="listeInfos">
				<li> Nom : <input type="text" name="nom" value="${personne.nom }"/> </li>
				<li> Pr�nom : <input type="text" name="prenom" value="${personne.prenom }"/> </li>
				<li> Bureau : <input type="text" name="bureau" value="${personne.bureau}"/> </li>
				<li> Poste : <input type="text" name="poste" value="${personne.poste}"/> </li>
				<li> Service : <input type="text" name="service" value="${personne.service}"/> </li>
				<c:if test="${not empty personne.chef}"><li> Chef : <a href="personne?id=${personne.chef.id}">${personne.chef.prenom} ${personne.chef.nom}</a></li></c:if>
				<li> ${bool } Nombre de descendants : ${personne.getNombreTotalDescendants()} dont ${personne.descendants.size()} directs</li>
			</ul>
			<div class="buttonContainer">
				<input type="submit" class="buttonSubmit"/>
			</div>
			</form>
		</c:when>
		<c:otherwise>
			<ul class="listeInfos">
				<li> Bureau : ${personne.bureau} </li>
				<li> Poste : ${personne.poste} </li>
				<li> Service : ${personne.service} </li>
				<c:if test="${not empty personne.chef}"><li> Chef : <a href="personne?id=${personne.chef.id}">${personne.chef.prenom} ${personne.chef.nom}</a></li></c:if>
				<li> ${bool } Nombre de descendants : ${personne.getNombreTotalDescendants()} dont ${personne.descendants.size()} directs</li>
			</ul>
		</c:otherwise>
	</c:choose>
	<p class="lien"><a href="accueil"> Retour � l'organigramme </a></p>
	</div>
</body>
</html>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Accueil</title>
<link rel=stylesheet type="text/css" href="style.css" />
</head>

<body>

	<c:choose>

		<c:when test="${ !empty sessionScope.identifiant }">

			Connecté en tant que ${ sessionScope.prenom  } ${ sessionScope.nom  } 
			<br />Dernière connexion : ${ sessionScope.lastConnexion } 
			<br />Nombre de connexion : ${ sessionScope.nbCo }
			<div class="identification">

				<form method="post" action="Accueil">
					<input type="submit" id="se déconnecter" value="Se déconnecter"
						class="submit" />
				</form>
			</div>

		</c:when>

		<c:when test="${ empty sessionScope.identifiant }">
			<form class="identification" method="post" action="Accueil">
				<p>
					<label for="identifiant">Identifiant : </label> <input type="text"
						name="identifiant" id="identifiant" />
				</p>

				<p>
					<label for="password">Mot de passe : </label> <input
						type="password" name="password" id="password" />
				</p>
				<input type="submit" id="se connecter" value="Se connecter" />
			</form>
		</c:when>
	</c:choose>

	<div class="treeContainer">
		<div class="tree">
			<ul>
				<li><a href="personne?id=${chef.id}"> ${chef.prenom}
						${chef.nom}</a> <c:if test="${not empty chef.descendants }">
						<ul>
							<c:forEach var="descendant" items="${chef.descendants}">
								<c:set var="chef" value="${descendant}" scope="request" />
								<jsp:include page="tree.jsp" />
							</c:forEach>
						</ul>
					</c:if></li>
			</ul>
		</div>
	</div>
</body>
</html>
package organigramme.beans;

import java.util.ArrayList;
import java.util.List;

public class Personne {
	
	private Integer id;
	private String nom;
	private String prenom;
	private String bureau;
	private String service;
	private String poste;
	private Personne chef;
	private String username;
	private String password;
	private List<Personne> descendants;
	
	
	
	public List<Personne> getDescendants() {
		return descendants;
	}
	
	public int getNombreTotalDescendants() {
		if(this.getDescendants().size()==0) return 0;
		int nb = this.getDescendants().size();
		for(Personne descendant : this.getDescendants()) {
			nb+=descendant.getNombreTotalDescendants();
		}
		return nb;
	}


	public Personne(String nom, String prenom, String bureau, String service, String poste, Personne chef, String username,
			String password) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.bureau = bureau;
		this.service = service;
		this.poste = poste;
		this.chef = chef;
		this.username = username;
		this.password = password;
		this.descendants=new ArrayList<Personne>();
	}
	
	public Personne() {
		this.nom="Default";
		this.prenom="Default";
		this.bureau="Default";
		this.service="Default";
		this.poste="Default";
		this.username="Default";
		this.password="Default";
		this.descendants=new ArrayList<Personne>();
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getBureau() {
		return bureau;
	}
	public void setBureau(String bureau) {
		this.bureau = bureau;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public String getPoste() {
		return poste;
	}
	public void setPoste(String poste) {
		this.poste = poste;
	}
	public Personne getChef() {
		return chef;
	}
	public void setChef(Personne chef) {
		this.chef = chef;
	}
}

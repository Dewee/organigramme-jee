package organigramme.beans;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CookiesGestionner {

	HttpServletRequest request; 
	HttpServletResponse response;

	DateFormat formatDate;
	DateFormat formatHours;
	Date calendar;
	String currentDate;
	String currentHours;
	
	

	public CookiesGestionner(HttpServletRequest request, HttpServletResponse response) {
		super();
		this.formatDate = new SimpleDateFormat("dd/MM/yyyy");
		this.formatHours = new SimpleDateFormat("HH:mm:ss");
		this.calendar = new Date();
		this.currentDate = formatDate.format(calendar.getTime());
		this.currentHours = formatHours.format(calendar.getTime());
		this.request = request;
		this.response = response;
	}

	public void createOrUpdateUserInfo(String cookieName) {
		Cookie[] cookies = request.getCookies();
		boolean exist = false;

		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(cookieName)) {
					exist = true;
					int count = Integer.parseInt(cookie.getValue()) + 1;
					createNewCookie(cookieName, String.valueOf(count));
				}
			}
		}

		if (!exist) {
			createNewCookie(cookieName, "1");
		}
		
		createOrUpdateDateUser(cookieName);
	}

	public void createOrUpdateDateUser(String cookieName) {
		createNewCookie(cookieName + "Date", this.currentDate);
		createNewCookie(cookieName + "Hours", this.currentHours);
	}

	public void createNewCookie(String cookieId, String value) {
		// Cr�ation des cookies
		Cookie cookie = new Cookie(cookieId, value);
		cookie.setMaxAge(2400 * 60 * 24 * 30);
		this.response.addCookie(cookie);
	}

	public boolean isExist(String coockieID) {
		Cookie[] cookies = this.request.getCookies();
		boolean state = false;

		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(coockieID)) {
					state = true;
				}
			}
		}
		return state;
	}
	
	public String getCookie(String coockieID) {
		Cookie[] cookies = this.request.getCookies();

		if (isExist(coockieID)) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(coockieID)) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}
}

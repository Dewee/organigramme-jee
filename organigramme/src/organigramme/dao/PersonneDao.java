package organigramme.dao;


import java.util.List;

import organigramme.beans.Personne;

public interface PersonneDao {
    void ajouter( Personne personne );
    List<Personne> lister();
	organigramme.beans.Personne getPersonneById(int id);
	void modifier(String id, String field, String value);
}
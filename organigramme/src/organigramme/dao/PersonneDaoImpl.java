package organigramme.dao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import organigramme.beans.Personne;

public class PersonneDaoImpl implements PersonneDao {
    private DaoFactory daoFactory;

    PersonneDaoImpl(DaoFactory daoFactory) {
        this.daoFactory = daoFactory;
    }

    @Override
    public void ajouter(Personne personne) {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("INSERT INTO personne(username,password,nom, prenom,bureau,service,poste,chef) VALUES(?, ?, ?, ?, ?, ?, ?, ?);");
            preparedStatement.setString(1, personne.getUsername());
            preparedStatement.setString(2, personne.getPassword());
            preparedStatement.setString(3, personne.getNom());
            preparedStatement.setString(4, personne.getPrenom());
            preparedStatement.setString(5, personne.getBureau());
            preparedStatement.setString(6, personne.getService());
            preparedStatement.setString(7, personne.getPoste());
            preparedStatement.setString(8, String.valueOf(personne.getChef().getId()));
            

            preparedStatement.executeUpdate();
            
            connexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Personne> lister() {
        List<Personne> personnes = new ArrayList<Personne>();
        Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;

        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT id,username,password,nom,prenom,bureau,service,poste,chef FROM personne;");

            while (resultat.next()) {
            	String id = resultat.getString("id");
            	String username = resultat.getString("username");
            	String password = resultat.getString("password");
                String nom = resultat.getString("nom");
                String prenom = resultat.getString("prenom");
                String bureau = resultat.getString("bureau");
                String service = resultat.getString("service");
                String poste = resultat.getString("poste");
                String chef = resultat.getString("chef");
     
                Personne personne = new Personne();
                personne.setId(Integer.valueOf(id));
                personne.setUsername(username);
                personne.setPassword(password);
                personne.setNom(nom);
                personne.setPrenom(prenom);
                personne.setBureau(bureau);
                personne.setService(service);
                personne.setPoste(poste);
                if(chef==null) personne.setChef(null);
                else {
                	personne.setChef(this.getPersonneById(Integer.valueOf(chef)));
                }

                personnes.add(personne);
                
                connexion.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personnes;
    }
    
    @Override
    public Personne getPersonneById(int id) {
    	Connection connexion = null;
        Statement statement = null;
        ResultSet resultat = null;
        Personne personne=new Personne();
        
        try {
            connexion = daoFactory.getConnection();
            statement = connexion.createStatement();
            resultat = statement.executeQuery("SELECT id,username,password,nom,prenom,bureau,service,poste,chef FROM personne WHERE id="+id+";");

            while (resultat.next()) {
            	String id2=resultat.getString("id");
            	String username = resultat.getString("username");
            	String password = resultat.getString("password");
                String nom = resultat.getString("nom");
                String prenom = resultat.getString("prenom");
                String bureau = resultat.getString("bureau");
                String service = resultat.getString("service");
                String poste = resultat.getString("poste");
                String chef = resultat.getString("chef");
     
                personne.setId(Integer.valueOf(id2));
                personne.setUsername(username);
                personne.setPassword(password);
                personne.setNom(nom);
                personne.setPrenom(prenom);
                personne.setBureau(bureau);
                personne.setService(service);
                personne.setPoste(poste);
                if(chef==null) personne.setChef(null);
                else {
                	personne.setChef(this.getPersonneById(Integer.valueOf(chef)));
                }
                
                connexion.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personne;
    }

	@Override
	public void modifier(String id, String field, String value) {
		Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            connexion = daoFactory.getConnection();
            preparedStatement = connexion.prepareStatement("UPDATE personne SET "+field+"=? WHERE id=?");
            preparedStatement.setString(1, value);
            preparedStatement.setString(2, id);
            

            preparedStatement.executeUpdate();
            
            connexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
		
	}


}
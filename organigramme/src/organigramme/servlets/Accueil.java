package organigramme.servlets;


import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import organigramme.beans.CookiesGestionner;
import organigramme.beans.Personne;
import organigramme.dao.DaoFactory;
import organigramme.dao.PersonneDao;

/**
 * Servlet implementation class Accueil
 */
@WebServlet("/Accueil")
public class Accueil extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private PersonneDao personneDao;

	public void init() throws ServletException {
		DaoFactory daoFactory = DaoFactory.getInstance();
		this.personneDao = daoFactory.getPersonneDao();
	}

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public Accueil() {
		super();
		try {
			init();
		} catch (ServletException e) {
			e.printStackTrace();
		}
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<Personne> personnes = personneDao.lister();
		Personne chef = null;
		for (Personne personne : personnes) {
			if (personne.getChef() == null)
				chef = personne;
			for (Personne personne2 : personnes) {
				if (personne2.getChef() != null) {
					if (personne2.getChef().getId() == personne.getId()) {
						personne.getDescendants().add(personne2);
					}
				}
			}
		}
		request.setAttribute("chef", chef);
		request.setAttribute("nb", personnes);
		this.getServletContext().getRequestDispatcher("/WEB-INF/accueil.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CookiesGestionner cookiesGestionner = new CookiesGestionner(request, response);
		List<Personne> personnes = personneDao.lister();
		boolean connexionState = false;
		String identifiant = request.getParameter("identifiant");
		String password;
		HttpSession session = request.getSession();

		if (request.getParameter("password") == null) {
			password = "";
		} else {
			password = request.getParameter("password");
		}

		for (Personne personne : personnes) {
			if (personne.getUsername().equals(identifiant) && personne.getPassword().equals(password)) {
				connexionState = true;

				session.setAttribute("identifiant", identifiant);
				session.setAttribute("prenom", personne.getPrenom());
				session.setAttribute("nom", personne.getNom());

				if (new CookiesGestionner(request, response).isExist(identifiant)) {
					String date = cookiesGestionner.getCookie(identifiant + "Date");
					String hours = cookiesGestionner.getCookie(identifiant + "Hours");
					session.setAttribute("lastConnexion", date + " � " + hours);
				}

				else {
					session.setAttribute("lastConnexion", "premi�re connexion");
				}

				cookiesGestionner.createOrUpdateUserInfo(identifiant);
				session.setAttribute("nbCo", cookiesGestionner.getCookie(identifiant));

			}
		}

		if (!connexionState) {
			session.invalidate();
		}
		doGet(request, response);
	}
	
}

package organigramme.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import organigramme.dao.DaoFactory;
import organigramme.dao.PersonneDao;

/**
 * Servlet implementation class Personne
 */
@WebServlet("/Personne")
public class Personne extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private PersonneDao personneDao;

    public void init() throws ServletException {
        DaoFactory daoFactory = DaoFactory.getInstance();
        this.personneDao = daoFactory.getPersonneDao();
    }   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Personne() {
        super();
        try {
			init();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		organigramme.beans.Personne p = null;
		List<organigramme.beans.Personne> personnes = personneDao.lister();
		for(organigramme.beans.Personne personne : personnes) {
			if(personne.getId()==Integer.valueOf(request.getParameter("id"))) p=personne;
    		for(organigramme.beans.Personne personne2 : personnes) {
    			if(personne2.getChef()!=null) {
    				if(personne2.getChef().getId().equals(personne.getId())) {
        				personne.getDescendants().add(personne2);
        			}
    			}
    		}
    	}
		request.setAttribute("personne",p);
        this.getServletContext().getRequestDispatcher("/WEB-INF/personne.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.personneDao.modifier(request.getParameter("id"), "nom", request.getParameter("nom"));
		this.personneDao.modifier(request.getParameter("id"),  "prenom",  request.getParameter("prenom"));
		this.personneDao.modifier(request.getParameter("id"), "bureau", request.getParameter("bureau"));
		this.personneDao.modifier(request.getParameter("id"), "service", request.getParameter("service"));
		this.personneDao.modifier(request.getParameter("id"), "poste", request.getParameter("poste"));
		doGet(request, response);
	}

}
